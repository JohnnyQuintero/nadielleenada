/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocio;
import Modelo.Libro;
import Util.ListaS;
/*
 *
 * @author JHONY QUINTERO
 */
public class Biblioteca {

    ListaS<Libro> lista = new ListaS();

    public Biblioteca() {

    }

    public void addLibro(Libro nuevo) {
        lista.insetarInicio(nuevo);
    }

    public String buscarLibro(String s) {
        String msg = "Libros segun su criterio de busqueda:\n"
                + "Su filtro se realizo a base de la palabra " + s + "\n";
        for (int i = 0; i < lista.getTamanio(); i++) {
            if (lista.get(i).getAutor().equals(s)
                    || lista.get(i).getTema().equals(s)
                    || lista.get(i).getTitulo().equals(s)) {
                msg += lista.get(i).toString() + "\n";
            }
        }
        return msg;
    }

    public void removeLibro(String s) {
        for (int i = 0; i < lista.getTamanio(); i++) {
            if (lista.get(i).getAutor().equals(s)
                    || lista.get(i).getTema().equals(s)
                    || lista.get(i).getTitulo().equals(s)) {
                lista.eliminar(i);
            }
        }
    }

    public String getListadoAutor() {
        String msg = "Listado Authores:\n";
        for (int i = 0; i < lista.getTamanio(); i++) {
            msg += lista.get(i).getAutor() + "\n";
        }
        return msg;
    }
//    public String getBuscarAlgo(int i) {
//
//	String msg=i==0?Autores”:”Tema”;
//        for (int i = 0; i < lista.getTamanio(); i++) {
//	  
//	String x= i==0:?ista.get(i).getTema(): lista.get(i).getTema();
//            msg += x+ "\n";
//        }
//        return msg;
//    }

    public String getListadoTema() {
        String msg = "Listado Authores:\n";
        for (int i = 0; i < lista.getTamanio(); i++) {
            msg += lista.get(i).getTema() + "\n";
        }
        return msg;
    }

    public String getListadoFecha(int dia, int mes, int anio) {
        String msg = "";
        for (int i = 0; i < lista.getTamanio(); i++) {
            if (lista.get(i).getDia() == dia || lista.get(i).getMes() == mes
                    || lista.get(i).getAnio() == anio) {
                msg += lista.toString() + "\n";
            }
        }
        return msg;
    }

    @Override
    public String toString() {
        return lista.toString();
    }
}
