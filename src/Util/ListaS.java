/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Util;

import Modelo.Libro;

/**
 *
 * @author JHONY QUINTERO
 * @param <T>
 */
public class ListaS<T> {

    private Nodo<T> cabeza;
    private int tam;

    public ListaS() {
        this.cabeza = null;
        this.tam = 0;
    }

    public void insetarInicio(T info) {
        this.cabeza = new Nodo(info, this.cabeza);
        this.tam++;
    }

    public T eliminar(int i) {
        if (this.esVacia()) {
            return null;
        }
        Nodo<T> t = this.cabeza;
        if (i == 0) {
            this.cabeza = this.cabeza.getSig();
        } else {
            try {
                Nodo<T> y = this.getPos(i - 1);
                t = y.getSig();
                y.setSig(t.getSig());
            } catch (Exception e) {
                System.err.println(e.getMessage());
                return (null);
            }
        }
        t.setSig(null);
        this.tam--;
        return (t.getInfo());
    }

//    public void eliminar(int i)
//    {
//        
//    }

    private int cmp(T info1, T info2) {
        Comparable cmp = (Comparable) info1;
        return cmp.compareTo(info2);
    }

    public T get(int i) {
        try {
            Nodo<T> t = this.getPos(i);
            return (t.getInfo());
        } catch (Exception e) {
            System.err.println(e.getMessage());
            return (null);
        }

    }

    public int getTamanio() {
        return (this.tam);
    }

    public boolean esVacia() {
        return (this.cabeza == null);
    }

    private Nodo<T> getPos(int i) throws RuntimeException {
        if (this.esVacia() || i > this.tam || i < 0) {
            throw new RuntimeException("El índice solicitado no existe en la Lista Simple");
        }
        Nodo<T> t = this.cabeza;
        while (i > 0) {
            t = t.getSig();
            i--;
        }
        return (t);
    }

    @Override
    public String toString() {
        String msg = "";
        for (Nodo<T> i = this.cabeza; i != null; i = i.getSig()) {
            msg += i.getInfo().toString() + "\n";
        }
        return msg;
    }
}
