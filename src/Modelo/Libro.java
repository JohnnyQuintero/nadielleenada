/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Objects;

/**
 *
 * @author JHONY QUINTERO
 */
public class Libro implements Comparable {

    private String autor, titulo, tema;
    private int anio, dia, mes;

    /**
     *
     */
    public Libro() {
    }

    public Libro(String autor, String titulo, String tema, int dia, int mes, int anio) {
        this.autor = autor;
        this.titulo = titulo;
        this.tema = tema;
        this.anio = anio;
        this.dia = dia;
        this.mes = mes;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 83 * hash + Objects.hashCode(this.autor);
        hash = 83 * hash + Objects.hashCode(this.titulo);
        hash = 83 * hash + Objects.hashCode(this.tema);
        hash = 83 * hash + this.anio;
        hash = 83 * hash + this.dia;
        hash = 83 * hash + this.mes;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Libro other = (Libro) obj;
        if (this.anio != other.anio) {
            return false;
        }
        if (this.dia != other.dia) {
            return false;
        }
        if (this.mes != other.mes) {
            return false;
        }

        return this.cmp(this.tema, other.titulo) && this.cmp(this.autor, other.autor)
                && this.cmp(this.titulo, other.titulo);
    }

    @Override
    public String toString() {
        return "Libro{" + "autor=" + autor + ", titulo=" + titulo + ", tema=" + tema + ", dia=" + dia + ", mes=" + mes + ", año=" + anio + '}';
    }

    @Override
    public int compareTo(Object o) {
        final Libro other = (Libro) o;
        if (this.titulo != other.titulo) {
            return this.titulo.compareTo(other.titulo);
        }
        if (this.tema != other.tema) {
            return this.tema.compareTo(other.tema);
        }
        if (this.dia != other.dia) {
            return this.dia - other.dia;
        }
        if (this.mes != other.mes) {
            return this.mes - other.mes;
        }
        if (this.dia != other.dia) {
            return this.dia - other.dia;
        }
        return this.autor.compareTo(other.autor);
    }

    public boolean cmp(String n1, String n2) {
        if (n1.length() != n2.length()) {
            return false;
        } else {
            for (int i = 0; i < n1.length(); i++) {
                if (n1.charAt(i) != n2.charAt(i)) {
                    return false;
                }
            }
        }
        return true;
    }
}
