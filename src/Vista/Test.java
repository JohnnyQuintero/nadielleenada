/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.Libro;
import Negocio.Biblioteca;

/**
 *
 * @author JHONY QUINTERO
 */
public class Test {

    public static void main(String[] args) {
        try {
            Biblioteca lis = new Biblioteca();
            Libro n1 = new Libro("Jhnny", "titanic1", "accion", 16, 12, 2001);
            Libro n2 = new Libro("Johny", "titanic2", "suspenso", 18, 11, 2000);
            Libro n3 = new Libro("Jnny", "titanic3", "ficion", 168, 2, 1990);
            lis.addLibro(n1);
            lis.addLibro(n2);
            lis.addLibro(n3);
            System.out.println("Libros Disponibles");
            System.out.println(lis.toString());

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }
}
